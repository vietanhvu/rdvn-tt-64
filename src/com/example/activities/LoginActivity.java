package com.example.activities;

import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import com.example.bitbucketclient.R;
import com.example.serverapi.BitbucketOAuthServiceFactory;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class LoginActivity extends BaseActivity {
	
	private static final String LOADING_HTML_PAGE = "<html><head></head><body></body></html>";
	
	protected WebView webView;
	protected OAuthService service;
	protected Token requestToken;
	protected String authUrl;
	protected ProgressBar progressBar;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		this.webView = (WebView) findViewById(R.id.login_webview);
		this.webView.setWebViewClient(new LoginWebViewClient());
		this.progressBar = (ProgressBar) findViewById(R.id.progressBar);
		WebSettings settings = this.webView.getSettings();
		settings.setSavePassword(false);
		settings.setSaveFormData(false);

		this.service = BitbucketOAuthServiceFactory.getOAuthService();
		new RequestTokenTask().execute();
	}
	
	protected class LoginWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (Uri.parse(url).getHost().equals(Uri.parse(BitbucketOAuthServiceFactory.CALLBACK).getHost())) {
				requestAccessToken(url);
			}
			if (url.equals(BitbucketOAuthServiceFactory.CANCEL_CALLBACK)) {
				LoginActivity.this.finish();
			}
			return false;
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			LoginActivity.this.webView.loadData(LOADING_HTML_PAGE, "text/html", null);
			progressBar.setVisibility(View.VISIBLE);

			AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
			builder.setPositiveButton(R.string.dialog_retry, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							LoginActivity.this.webView.loadUrl(LoginActivity.this.authUrl);
							progressBar.setVisibility(View.VISIBLE);
						}
					});
			builder.setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							LoginActivity.this.finish();
						}
					});
			AlertDialog dialog = builder.create();
			dialog.setTitle(R.string.dialog_error_title);
			dialog.setMessage(getString(R.string.dialog_oauth_load_failed));
			dialog.show();
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);
			progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			progressBar.setVisibility(View.GONE);
		}
	}
	
	protected void requestAccessToken(String url) {
		if (this.service == null || this.requestToken == null) {
			new RequestTokenTask().execute();
			return;
		}
		new AccessTokenTask().execute(url);
	}
	
	protected class RequestTokenTask extends AsyncTask<Void, Void, String> {
		@Override
		protected void onPreExecute() {
			webView.loadData(LOADING_HTML_PAGE, "text/html", null);
			progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				requestToken = service.getRequestToken();
				authUrl = service.getAuthorizationUrl(requestToken);
				System.out.println("request token ============= " + requestToken.toString());
				System.out.println("authUrl ============= " + authUrl);
				return authUrl;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}

		@SuppressLint("SetJavaScriptEnabled")
		@Override
		protected void onPostExecute(String authUrlParameter) {
			if (authUrlParameter != null) {
				webView.getSettings().setJavaScriptEnabled(true);// needed in two factor Google authentication
				webView.loadUrl(authUrlParameter);
				progressBar.setVisibility(View.VISIBLE);
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>> loading");
			} else {
				AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
				builder.setPositiveButton(R.string.dialog_retry, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								new RequestTokenTask().execute();
							}
						});
				builder.setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								LoginActivity.this.finish();
							}
						});
				AlertDialog dialog = builder.create();
				dialog.setTitle(R.string.dialog_error_title);
				dialog.setMessage(getString(R.string.dialog_oauth_load_failed));
				dialog.show();
			}
		}
	}

	protected class AccessTokenTask extends AsyncTask<String, Void, Token> {

		@Override
		protected void onPreExecute() {
			webView.getSettings().setJavaScriptEnabled(false);
			webView.loadData(LOADING_HTML_PAGE, "text/html", null);
			progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected Token doInBackground(String... params) {
			try {
				String url = params[0];
				Uri uri = Uri.parse(url);
				String parameter = uri.getEncodedQuery();
				parameter = parameter.split("oauth_verifier=")[1];
				parameter = parameter.split("&")[0];
				Token accessToken = service.getAccessToken(requestToken, new Verifier(parameter));
				return accessToken;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}

		@Override
		protected void onPostExecute(Token accessToken) {
			if (accessToken != null) {
				application.setAccessToken(accessToken);

				// Close down the session which was used during authentication.
				// All communication will use OAuth from this point forward
				webView.loadUrl("https://bitbucket.org/account/signout/");
				progressBar.setVisibility(View.VISIBLE);

				finish();
			} else {
				AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
				builder.setPositiveButton(R.string.dialog_retry, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								new AccessTokenTask().execute();
							}
						});
				builder.setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								LoginActivity.this.finish();
							}
						});
				AlertDialog dialog = builder.create();
				dialog.setTitle(R.string.dialog_error_title);
				dialog.setMessage(getString(R.string.dialog_oauth_load_failed));
				dialog.show();
			}
		}
	}
}
