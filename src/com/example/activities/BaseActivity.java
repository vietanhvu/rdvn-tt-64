package com.example.activities;

import com.example.application.ApplicationContext;
import com.example.bitbucketclient.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class BaseActivity extends Activity {
	
	protected ApplicationContext application;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		application = new ApplicationContext(this);
	}
	
	protected void onResume() {
		super.onResume();
		loadData(1);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);

		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.logout:
			logOut();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	public ApplicationContext getApplicationPreferences() {
		return application;
	}
	
	protected void logOut() {
		application.clear();
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
		finish();
	}
	
	public void requestDataSuccess(String data) {}
	
	public void requestDataFail() {}
	
	public void loadData(int i) {}
}
