package com.example.activities;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.adapter.CommitsAdapter;
import com.example.model.Commit;
import com.example.bitbucketclient.R;
import com.example.serverapi.AsyncLoader;
import com.google.gson.Gson;

public class CommitsActivity extends BaseActivity {
	
	private String repoApiUri;
	
	private String branchName;
	
	private int currentPage = 1;
	
	private boolean loading = true;
	
	private boolean endOfList = false;
	
	private final List<Commit> commits = new ArrayList<Commit>();
	
	private CommitsAdapter adapter;
	
	private ProgressBar progressBar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_commits);
		setTitle(R.string.commits_title);
		
		repoApiUri = getIntent().getStringExtra("extra_api_uri").replaceFirst("1.0", "2.0");
		branchName = getIntent().getStringExtra("extra_branch_name");
		
		commits.clear();
		progressBar = (ProgressBar) findViewById(R.id.progress_bar);
		ListView view = (ListView)findViewById(R.id.commits_view);
		adapter = new CommitsAdapter(this, commits);
		view.setAdapter(adapter);
		view.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				if (firstVisibleItem + visibleItemCount >= totalItemCount && !loading && !endOfList) {
					loadData(currentPage+1);
				}
			}

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub

			}
		});

	}

	@Override
	public void loadData(int page) {
		loading = true;
		progressBar.setVisibility(View.VISIBLE);
		String pageParam = "?page=" + String.valueOf(page);
		new AsyncLoader(this, application.getAccessToken())
		.execute(repoApiUri + "/commits/" + branchName + pageParam);
	}

	@Override
	public void requestDataSuccess(String data) {
		loading = false;
		progressBar.setVisibility(View.GONE);
		JSONObject object;
		try {
			object = new JSONObject(data);
			int page = object.getInt("page");
			JSONArray listCommits = object.getJSONArray("values");
			if (listCommits.length() == 0) {
				endOfList = true;
				currentPage = page - 1;
				return;
			}
			endOfList = false;
			currentPage = page;
			
			for (int i = 0; i < listCommits.length(); i++) {
				Gson gson = new Gson();
				Commit commit = gson.fromJson(listCommits.get(i).toString(), Commit.class);
				commits.add(commit);
			}
			System.out.println("commits size ============================ " + commits.size());
			adapter.notifyDataSetChanged();
		} catch (JSONException e) {
			//
		}
	}
	
	@Override
	public void requestDataFail() {
		loading = false;
		progressBar.setVisibility(View.GONE);
	}
}
