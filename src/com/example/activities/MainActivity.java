package com.example.activities;

import org.json.JSONObject;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import com.example.bitbucketclient.R;
import com.example.serverapi.BitbucketOAuthServiceFactory;
import com.example.serverapi.ServerApiConstants;

import android.os.AsyncTask;
import android.os.Bundle;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;

public class MainActivity extends BaseActivity {

	public enum LoginState {
		ALL_FINE, AUTH_FAIL, NO_ACCESS_TOKEN
	}
	
	private ProgressBar progress_bar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Button btn = (Button) findViewById(R.id.login_button);
		progress_bar = (ProgressBar) findViewById(R.id.progress_bar);

		btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this,
						LoginActivity.class);
				startActivity(intent);
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		progress_bar.setVisibility(View.VISIBLE);
		new CheckLogin().execute();
	}
	
	@Override
	public void onBackPressed() {
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}
	
	protected void loggedIn() {
		Intent intent = new Intent(this, RepositoriesActivity.class);
		startActivity(intent);
		finish();
	}
	
	@Override
	protected void logOut() {
		application.clear();
	}

	protected class CheckLogin extends AsyncTask<String, Integer, LoginState> {
		@Override
		protected void onPreExecute() {}

		@Override
		protected LoginState doInBackground(String... params) {
			Token accessToken = application.getAccessToken();
			if (accessToken == null) {
				return LoginState.NO_ACCESS_TOKEN;
			}
			try {
				OAuthRequest request = new OAuthRequest(Verb.GET, ServerApiConstants.API_BASE_URL
						+ "/user/");
				OAuthService service = BitbucketOAuthServiceFactory
						.getOAuthService();
				service.signRequest(accessToken, request);
				Response response = request.send();
				String username = new JSONObject(response.getBody())
						.getJSONObject("user").getString("username");
				application.setUsername(username);
				return LoginState.ALL_FINE;
			} catch (Exception e) {
				return LoginState.AUTH_FAIL;
			}
		}

		@Override
		protected void onPostExecute(LoginState result) {
			progress_bar.setVisibility(View.GONE);
			switch (result) {
			case ALL_FINE:
				loggedIn();
				break;
			case NO_ACCESS_TOKEN:
				break;
			case AUTH_FAIL:
				logOut();
				break;
			default:
				logOut();
				break;
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
}
