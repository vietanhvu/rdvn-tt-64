package com.example.activities;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import com.example.adapter.ReposAdapter;
import com.example.model.Repository;
import com.example.bitbucketclient.R;
import com.example.serverapi.AsyncLoader;
import com.example.serverapi.ServerApiConstants;
import com.google.gson.Gson;

import android.os.Bundle;
import android.widget.ListView;

public class RepositoriesActivity extends BaseActivity {

	private List<Repository> repositories = new ArrayList<Repository>();
	
	private ListView listView;
	
	private ReposAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_repositories);
		setTitle(R.string.repositories_title);
		adapter = new ReposAdapter(this, repositories);
		listView = (ListView) findViewById(R.id.list_repos);
		listView.setAdapter(adapter);

	}

	@Override
	public void loadData(int page) {
		new AsyncLoader(this, application.getAccessToken())
				.execute((ServerApiConstants.API_BASE_URL + "/user/repositories/"));
	}

	@Override
	public void requestDataSuccess(String data) {
		try {
			JSONArray listRepos = new JSONArray(data);
			repositories.clear();
			for (int i = 0; i < listRepos.length(); i++) {
				Gson gson = new Gson();
				Repository repo = gson.fromJson(listRepos.get(i).toString(), Repository.class);
				repositories.add(repo);
			}
			adapter.notifyDataSetChanged();
		} catch (JSONException e) {
			//
		}
	}
	
	@Override
	public void requestDataFail() {}
}
