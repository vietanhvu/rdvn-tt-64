package com.example.activities;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.adapter.BranchesAdapter;
import com.example.model.Repository;
import com.example.bitbucketclient.R;
import com.example.serverapi.AsyncLoader;
import com.example.serverapi.ServerApiConstants;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class BranchesActivity extends BaseActivity {
	
	private List<String> branches = new ArrayList<String>();
	
	private Repository repository;
	
	private BranchesAdapter adapter;
	
	private String repoApiUri;
	
	private ProgressBar progressBar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_branches);
		setTitle(R.string.branches_title);
		
		repository = getIntent().getParcelableExtra("extra_repo_info");
		repoApiUri = ServerApiConstants.BASE_URL + repository.getUri();
		
		ListView view = (ListView) findViewById(R.id.repo_branches_view);
		adapter = new BranchesAdapter(this, branches, repoApiUri);
		view.setAdapter(adapter);
		
		progressBar = (ProgressBar) findViewById(R.id.progress_bar);
		TextView name = (TextView) findViewById(R.id.repo_info_name);
		name.setText(repository.getName());
		TextView access_level = (TextView) findViewById(R.id.repo_info_scope);
		access_level.setText(repository.getAccessLevel());
		TextView owner = (TextView) findViewById(R.id.repo_info_owner);
		owner.setText(repository.getOwner());
		
	}
	
	@Override
	public void loadData(int page) {
		progressBar.setVisibility(View.VISIBLE);
		new AsyncLoader(this, application.getAccessToken())
		.execute(repoApiUri + "/branches");
	}

	@Override
	public void requestDataSuccess(String data) {
		progressBar.setVisibility(View.GONE);
		JSONObject object;
		try {
			object = new JSONObject(data);
			JSONArray listBranches = object.names();
			branches.clear();
			for (int k = 0; k < listBranches.length(); k++) {
				branches.add(listBranches.getString(k));
			}
			System.out.println("branch size: " + branches.size());
			adapter.notifyDataSetChanged();
		} catch (JSONException e) {
			//
		}
	}
	
	@Override
	public void requestDataFail() {
		progressBar.setVisibility(View.GONE);
	}
}
