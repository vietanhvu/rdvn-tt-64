package com.example.adapter;

import java.util.List;

import com.example.activities.BaseActivity;
import com.example.model.Commit;
import com.example.bitbucketclient.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CommitsAdapter extends BaseAdapter {

	private BaseActivity activity;

	private final List<Commit> commits;

	private static LayoutInflater inflater = null;

	public CommitsAdapter(BaseActivity activity, List<Commit> commits) {
		// TODO Auto-generated constructor stub
		this.activity = activity;
		this.commits = commits;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return commits.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final Commit commit = commits.get(position);
		
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.commits_item, null);
			TextView author = (TextView)convertView.findViewById(R.id.commit_author);
			TextView hash = (TextView)convertView.findViewById(R.id.commit_hash);
			TextView message = (TextView)convertView.findViewById(R.id.commit_message);
			TextView date = (TextView)convertView.findViewById(R.id.commit_date);
			
			if (commit.getAuthor().getUser() != null)
				author.setText(commit.getAuthor().getUser().getDisplayName());
			else
				author.setText(commit.getAuthor().getRaw());
			hash.setText(commit.getHash());
			message.setText(commit.getMessage());
			date.setText(commit.getDate());
		} else {
			View view = (View) convertView;
			TextView author = (TextView)view.findViewById(R.id.commit_author);
			TextView hash = (TextView)view.findViewById(R.id.commit_hash);
			TextView message = (TextView)view.findViewById(R.id.commit_message);
			TextView date = (TextView)view.findViewById(R.id.commit_date);
			
			if (commit.getAuthor().getUser() != null)
				author.setText(commit.getAuthor().getUser().getDisplayName());
			else
				author.setText(commit.getAuthor().getRaw());
			hash.setText(commit.getHash());
			message.setText(commit.getMessage());
			date.setText(commit.getDate());
		}

		return convertView;
	}

}
