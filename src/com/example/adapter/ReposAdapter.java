package com.example.adapter;

import java.util.List;

import com.example.activities.BaseActivity;
import com.example.activities.BranchesActivity;
import com.example.model.Repository;
import com.example.bitbucketclient.R;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ReposAdapter extends BaseAdapter {
	
	private BaseActivity activity;
	
	private final List<Repository> repositories;
	
	private static LayoutInflater inflater = null;

	public ReposAdapter(BaseActivity repositoriesActivity,
			List<Repository> repositories) {
		// TODO Auto-generated constructor stub
		this.activity = repositoriesActivity;
		this.repositories = repositories;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return repositories.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final Repository item = repositories.get(position);
		
		if (convertView == null) {
			convertView = inflater.inflate(com.example.bitbucketclient.R.layout.repos_item, null);
			TextView owner = (TextView) convertView.findViewById(R.id.repo_owner);
			TextView name = (TextView) convertView.findViewById(R.id.repo_name);
			
			System.out.println(repositories.size() + "    " + item.getOwner());
			
			owner.setText(item.getOwner());
			name.setText(item.getName());
			
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(activity, BranchesActivity.class);
					intent.putExtra("extra_repo_info", item);
					activity.startActivity(intent);
				}
			});
		} else {
			View view = (View) convertView;
			TextView owner = (TextView) view.findViewById(R.id.repo_owner);
			TextView name = (TextView) view.findViewById(R.id.repo_name);
			
			owner.setText(item.getOwner());
			name.setText(item.getName());
		}
		
		return convertView;
	}

}
