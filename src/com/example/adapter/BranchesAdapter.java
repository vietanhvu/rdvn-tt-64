package com.example.adapter;

import java.util.ArrayList;
import java.util.List;

import com.example.activities.BaseActivity;
import com.example.activities.CommitsActivity;

import com.example.bitbucketclient.R;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class BranchesAdapter extends BaseAdapter {

	private List<String> branches = new ArrayList<String>();

	private BaseActivity activity;
	
	private String repoApiUri;

	public BranchesAdapter(BaseActivity activity, List<String> branches,
			String repoApiUri) {
		// TODO Auto-generated constructor stub
		this.activity = activity;
		this.branches = branches;
		this.repoApiUri = repoApiUri;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return branches.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final String name = branches.get(position);
		if (convertView == null) {
			TextView view = new TextView(activity);
			
			view.setText(name);
			view.setTextSize(16);
			view.setTextColor(activity.getResources().getColor(R.color.light_gray));
			view.setPadding(0, 5, 0, 5);
			view.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(activity, CommitsActivity.class);
					intent.putExtra("extra_branch_name", name);
					intent.putExtra("extra_api_uri", repoApiUri);
					activity.startActivity(intent);
				}
			});
			return view;
		} else {
			TextView view = (TextView) convertView;
			view.setText(name);
		}
		
		return convertView;
	}

}
