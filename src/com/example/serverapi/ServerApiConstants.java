package com.example.serverapi;

public interface ServerApiConstants {
	
	public static final String API_BASE_URL = "https://bitbucket.org/api/1.0";
	
	public static final String BASE_URL = "https://bitbucket.org/api";
	
	public static final String GET_METHOD = "GET";
	
	public static final String POST_METHOD = "POST";
}
