package com.example.serverapi;

import org.scribe.builder.api.DefaultApi10a;
import org.scribe.model.Token;

public class BitbucketOAuthApi extends DefaultApi10a
{
	private static String AccessTokenEndpoint = "https://bitbucket.org/!api/1.0/oauth/access_token";
	
	private static String AuthorizationUrl = "https://bitbucket.org/!api/1.0/oauth/authenticate?oauth_token=";
	
	private static String RequestTokenEndpoint = "https://bitbucket.org/!api/1.0/oauth/request_token";
	
	@Override
	public String getAccessTokenEndpoint()
	{
		return AccessTokenEndpoint;
	}
	
	@Override
	public String getAuthorizationUrl(Token requestToken)
	{
		return AuthorizationUrl + requestToken.getToken();
	}
	
	@Override
	public String getRequestTokenEndpoint()
	{
		return RequestTokenEndpoint;
	}
}
