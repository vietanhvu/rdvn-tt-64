package com.example.serverapi;

import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import com.example.activities.BaseActivity;
import com.example.activities.LoginActivity;
import com.example.application.ApplicationContext;
import com.example.bitbucketclient.R;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.os.AsyncTask;

public class AsyncLoader extends AsyncTask<String, Integer, String> {

	private String http_method = "GET";
	private static final String FAIL = "Fail";
	private static final String AUTH_FAIL = "Auth-Fail";
	
	private Token accessToken;
	
	protected ApplicationContext application;
	protected BaseActivity activity;

	public AsyncLoader(BaseActivity activity, Token accessToken) {
		this.accessToken = accessToken;
		this.activity = activity;
		application = activity.getApplicationPreferences();
	}

	@Override
	protected String doInBackground(String... params) {
		String apiUrl = params[0];
		String postParams = null;
		if (params.length >= 2) {
			postParams = params[1];
		}
		if (params.length == 4) {
			if (params[3] != null)
				http_method = params[3];
		}
		
		String cache = application.getKV(apiUrl);
		if (http_method.equals("GET") && cache != null) {
			return cache;
		}
		
		try {
			OAuthRequest request;
			if (http_method.equals("DELETE")) {
				request = new OAuthRequest(Verb.DELETE, apiUrl);
			} else if (http_method.equals("POST")) {
				request = new OAuthRequest(Verb.POST, apiUrl);
			} else if (http_method.equals("PUT")) {
				request = new OAuthRequest(Verb.PUT, apiUrl);
			} else {
				request = new OAuthRequest(Verb.GET, apiUrl);
			}
			if (postParams != null) {
				String[] params2 = postParams.split("&");
				for (int i = 0; i < params2.length; i++) {
					String[] param = params2[i].split("=");
					if (param.length >= 2) {
						if (param.length > 2) {
							for (int j = 2; j < param.length; j++) {
								param[1] += param[j];
							}
						}
						request.addBodyParameter(param[0], param[1]);
					}
				}
			}
			if (this.accessToken != null) {
				OAuthService service = BitbucketOAuthServiceFactory
						.getOAuthService();
				service.signRequest(this.accessToken, request);
			}
			System.out.println("request ====================== " + request.getCompleteUrl());
			Response response = request.send();
			if (response.getCode() == 401 || response.getCode() == 403) {
				return AUTH_FAIL;
			}
			String result = response.getBody();
			if (http_method.equals("GET") && !result.isEmpty()
					&& result.length() < 100000) {
				application.setKV(apiUrl, result);
			}
			
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return FAIL;
		}

	}

	@Override
	protected void onPostExecute(String result) {
		if (result.equals(AUTH_FAIL)) {
			try {
				AlertDialog alertDialog = new AlertDialog.Builder(activity)
						.create();
				alertDialog.setOnDismissListener(new OnDismissListener() {

					@Override
					public void onDismiss(DialogInterface dialog) {
						Intent intent = new Intent(activity, LoginActivity.class);
						activity.startActivity(intent);
						activity.finish();
					}
				});
				alertDialog.setTitle(R.string.dialog_error_title);
				alertDialog.setMessage(activity
						.getString(R.string.dialog_oauth_not_authorized));
				alertDialog.show();
			} catch (Exception e) {
			}

		} else if (result.equals(FAIL)) {
			try {
				AlertDialog alertDialog = new AlertDialog.Builder(activity)
						.create();
				alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,
						activity.getText(R.string.text_retry),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								// TODO Auto-generated method stub
								activity.loadData(1);
							}
						});
				
				alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
						activity.getText(R.string.text_cancel),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								// TODO Auto-generated method stub
								activity.requestDataFail();
							}
						});
				alertDialog.setTitle(R.string.dialog_error_title);
				alertDialog.setMessage(activity
						.getString(R.string.dialog_data_load_failed));
				alertDialog.show();
			} catch (Exception e) {
			}
		} else {
			activity.requestDataSuccess(result);
		}
	}
}
