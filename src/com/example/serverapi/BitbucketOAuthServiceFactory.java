package com.example.serverapi;

import org.scribe.builder.ServiceBuilder;
import org.scribe.oauth.OAuthService;

public class BitbucketOAuthServiceFactory
{
	private static OAuthService service;
	public final static String CALLBACK = "http://localhost/";
	public final static String CANCEL_CALLBACK = "https://bitbucket.org/";
	
	public static synchronized OAuthService getOAuthService()
	{
		if(service == null)
		{
			ServiceBuilder builder = new ServiceBuilder();
			builder.provider(BitbucketOAuthApi.class);
			/*
			IMPORTANT INFO:
			To access BitBucket via OAuth it is necessary to register an OAuth consumer with its own key and secret.
			
			For your own developing, register your own consumer.
			You will then receive a key and a secret from BitBucket for your application.
			More detailed information here:
			https://confluence.atlassian.com/display/BITBUCKET/OAuth+on+Bitbucket
			
			!DO NEVER EVER SHARE YOUR KEY OR YOUR SECRET WITH YOUR SOURCE CODE!
			
			*/
			builder.apiKey("nBGyzsXKgNSGQHEhNC");
			builder.apiSecret("98fumxeSZjuKxucG38kEfHHaaRGzu9zW");
			builder.callback(CALLBACK);
			service = builder.build();
		}
		return service;
	}
}
