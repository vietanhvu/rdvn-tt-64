package com.example.application;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.scribe.model.Token;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class ApplicationContext extends Application {

	private SharedPreferences sharedPreferences;

	/** A cache of key-value -pairs. */
	private Map<String, String> kv = new LinkedHashMap<String, String>();

	/** The cache dir. */
	private File cacheDir;

	private static final int MAX_CACHE_ITEMS = 100;

	public ApplicationContext(Context context) {
		sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		kv = new LinkedHashMap<String, String>();
		// Find the dir to save cached images
		if (android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED))
			cacheDir = new File(
					android.os.Environment.getExternalStorageDirectory(),
					"bitbucketclient/data");
		else
			cacheDir = getApplicationContext().getCacheDir();
		if (!cacheDir.exists())
			cacheDir.mkdirs();
	}

	public synchronized void setKV(String key, String value) {
		kv.put(key, value);
		// Remove the oldest entries from the cache, if there are too
		// many of them:
		while (kv.size() > MAX_CACHE_ITEMS) {
			Entry<String, String> entry = kv.entrySet().iterator().next();
			kv.remove(entry.getKey());
		}
	}

	public String getKV(String key) {
		return kv.get(key);
	}

	public void delKV(String key) {
		kv.remove(key);
	}

	public void clearKV() {
		kv.clear();
	}

	public SharedPreferences getSharedPreferences() {
		return sharedPreferences;
	}

	public Token getAccessToken() {
		String token = sharedPreferences.getString("accessToken_Token", null);
		if (token == null)
			return null;
		String secret = sharedPreferences.getString("accessToken_Secret", null);
		if (secret == null)
			return null;
		String rawResponse = sharedPreferences.getString(
				"accessToken_RawResponse", null);
		return new Token(token, secret, rawResponse);
	}

	public void setAccessToken(Token accessToken) {
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString("accessToken_Token", accessToken.getToken());
		editor.putString("accessToken_Secret", accessToken.getSecret());
		editor.putString("accessToken_RawResponse",
				accessToken.getRawResponse());
		editor.commit();
	}

	public String getUsername() {
		return sharedPreferences.getString("username", "");
	}

	public void setUsername(String username) {
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString("username", username);
		editor.commit();
	}

	public void clear() {
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.remove("accessToken_Token");
		editor.remove("accessToken_Secret");
		editor.remove("accessToken_RawResponse");
		editor.remove("username");
		editor.commit();
		clearKV();
	}
}
