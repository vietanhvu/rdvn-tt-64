package com.example.model;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
	
	@SerializedName("username")
	private String username;
	
	@SerializedName("display_name")
	private String display_name;
	
	public User(String username, String display_name) {
		this.username = username;
		this.display_name = display_name;
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getDisplayName() {
		return display_name;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(username);
		dest.writeString(display_name);
	}

	public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
		@Override
		public User createFromParcel(Parcel in) {
			return new User(in);
		}

		@Override
		public User[] newArray(int size) {
			return new User[size];
		}
	};

	private User(Parcel in) {
		username = in.readString();
		display_name = in.readString();
	}
}
