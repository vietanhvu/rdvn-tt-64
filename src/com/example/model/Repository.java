package com.example.model;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

public class Repository implements Parcelable {

	@SerializedName("scm")
	private String scm;

	@SerializedName("owner")
	private String owner;

	@SerializedName("state")
	private String state;

	@SerializedName("description")
	private String description;

	@SerializedName("resource_uri")
	private String resource_uri;
	
	@SerializedName("slug")
	private String slug;
	
	@SerializedName("is_private")
	private boolean is_private;

	@SerializedName("creator")
	private String creator;

	@SerializedName("name")
	private String name;

	public Repository(String name, String owner, String resource_uri, String slug,
			String description, String creator, String state, String scm,
			boolean is_private) {
		this.name = name;
		this.owner = owner;
		this.state = state;
		this.resource_uri = resource_uri;
		this.creator = creator;
		this.description = description;
		this.scm = scm;
		this.slug = slug;
		this.is_private = is_private;
	}
	
	public String getOwner() {
		return this.owner;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getUri() {
		return this.resource_uri;
	}
	
	public String getSlug() {
		return this.slug;
	}
	
	public String getAccessLevel() {
		if (is_private)
			return "Private";
		return "Public";
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(name);
		dest.writeString(owner);
		dest.writeString(state);
		dest.writeString(resource_uri);
		dest.writeString(creator);
		dest.writeString(description);
		dest.writeString(scm);
		dest.writeString(slug);
		dest.writeByte((byte) (is_private ? 1 : 0));
	}

	public static final Parcelable.Creator<Repository> CREATOR = new Parcelable.Creator<Repository>() {
		@Override
		public Repository createFromParcel(Parcel in) {
			return new Repository(in);
		}

		@Override
		public Repository[] newArray(int size) {
			return new Repository[size];
		}
	};

	private Repository(Parcel in) {
		name = in.readString();
		owner = in.readString();
		state = in.readString();
		resource_uri = in.readString();
		creator = in.readString();
		description = in.readString();
		scm = in.readString();
		slug = in.readString();
		is_private = in.readByte() == 1;
	}
}
