package com.example.model;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

public class Author implements Parcelable {
	
	@SerializedName("raw")
	private String raw;
	
	@SerializedName("user")
	private User user;
	
	public Author(String raw, User user) {
		this.raw = raw;
		this.user = user;
	}
	
	public String getRaw() {
		return this.raw;
	}
	
	public User getUser() {
		return this.user;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(raw);
		dest.writeParcelable(user, flags);
	}

	public static final Parcelable.Creator<Author> CREATOR = new Parcelable.Creator<Author>() {
		@Override
		public Author createFromParcel(Parcel in) {
			return new Author(in);
		}

		@Override
		public Author[] newArray(int size) {
			return new Author[size];
		}
	};

	private Author(Parcel in) {
		raw = in.readString();
		user = in.readParcelable(User.class.getClassLoader());
	}
}
