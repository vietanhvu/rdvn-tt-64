package com.example.model;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

public class Commit implements Parcelable {
	
	@SerializedName("author")
	private Author author;
	
	@SerializedName("message")
	private String message;
	
	@SerializedName("hash")
	private String hash;
	
	@SerializedName("date")
	private String date;
	
	public Commit(Author author, String message, String hash, String date) {
		this.author = author;
		this.message = message;
		this.hash = hash;
		this.date = date;
	}
	
	public Author getAuthor() {
		return author;
	}
	
	public String getMessage() {
		return message;
	}
	
	public String getHash() {
		return hash;
	}
	
	public String getDate() {
		return date;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeParcelable(author, flags);
		dest.writeString(message);
		dest.writeString(hash);
		dest.writeString(date);
	}

	public static final Parcelable.Creator<Commit> CREATOR = new Parcelable.Creator<Commit>() {
		@Override
		public Commit createFromParcel(Parcel in) {
			return new Commit(in);
		}

		@Override
		public Commit[] newArray(int size) {
			return new Commit[size];
		}
	};

	private Commit(Parcel in) {
		author = in.readParcelable(Author.class.getClassLoader());
		message = in.readString();
		hash = in.readString();
		date = in.readString();
	}
}
